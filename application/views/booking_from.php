<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Booking form</title>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">


<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>


</head>
<body>
 
 	<div id="container">
 		<br>
		<h1>Booking form</h1>
		<form>
		<div class="row ">
			<div class="col-sm-3 form-group">
				<label>Name</label>
				<input type="text" name="name" id='name' class="form-control">
			</div>
			<div class="col-sm-3 form-group">
				<label>Mobile no</label>
				<input type="text" name="mobile_no" id='mobile_no' class="form-control">
			</div>
			<div class="col-sm-3 form-group">
				<label>Email</label>
				<input type="text" name="email" id='email' class="form-control">
			</div>

		</div>
		<hr>
		<div class="row">
			<div class="col-sm-3 form-group">
				<label>Day</label>
				<br>
				<div class="checkbox-inline">
				<label>All<input type="checkbox" name="day[]" id='' class="form-control" value="all"></label>
				<label>Monday<input type="checkbox" name="day[]" id='name' class="form-control" value="monday"></label>
				<label>Tuseday<input type="checkbox" name="day[]" id='Tuseday' class="form-control" value="Tuseday"></label>
				<label>Wednesday<input type="checkbox" name="day[]" id='name' class="form-control" value="Wednesday"></label>
				<label>Thursday<input type="checkbox" name="day[]" id='name' class="form-control" value="Thursday"></label>
				<label>Friday<input type="checkbox" name="day[]" id='name' class="form-control" value="friday"></label>
				<label>Saturday<input type="checkbox" name="day[]" id='name' class="form-control" value="saturday"></label>
				<label>Sunday<input type="checkbox" name="day[]" id='name' class="form-control" value="sunday"></label>
			</div>
			</div>
			
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-3 form-group">
				<label>Timeslots</label>
				<div class="input-group bootstrap-timepicker timepicker pull-right">
            <input id="timepicker" type="text" class="form-control input-small" name='time'>
        </div>
			</div>
			<div class="col-sm-3 form-group">
				<label>Applay form date</label>
				<div class="input-group date" data-provide="datepicker">
			    <input type="text" class="form-control" name='applay_date'>
			    <div class="input-group-addon">
			        <span class="glyphicon glyphicon-th"></span>
			    </div>
			</div>

			</div>
			<div class="col-sm-3">
				<label>Comments</label>
				<input type="text" name="email" id='email' class="form-control">
			</div>
			<div class="col-sm-12 center">
			<button type="submit">Submit</button>
			</div>
		</div>
		</form>
    </div>
</body>
</html>
<script type="text/javascript">
$(function() {
  $('#date').datetimepicker();
  $('#timepicker').timepicker({
    timeFormat: 'h:mm p',
    interval: 60,
    minTime: '10',
    maxTime: '6:00pm',
    defaultTime: '11',
    startTime: '10:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
});
});
</script>
